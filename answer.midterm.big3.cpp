#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <iomanip>

using namespace std;

template<typename T>
class MyArray {
  friend ostream& operator<< (ostream& out, const MyArray & s )  ;
private:
  T* m_arr = nullptr ; 
  size_t m_size = 0 ;

public:
   // CSTR
   MyArray (  size_t n ) {
      cout << __PRETTY_FUNCTION__ << endl;
      m_size = n;
      m_arr = new int [n];
      fill(m_arr,m_arr+n,0);
   };

   // Rules of Big 3: DSTR
   ~MyArray (  ) {
      cout << __PRETTY_FUNCTION__ << endl;
      delete m_arr;
      m_arr = nullptr; 
      m_size= 0 ;
   };

   // Rules of Big 3: Copy Cstr
   MyArray (  const MyArray& x )  {
      cout << __PRETTY_FUNCTION__ << endl;
      size_t n = m_size = x.m_size;
      m_arr = new int [n];
      copy (x.m_arr, x.m_arr+n, m_arr );
    }

  // Rules of Big 3: assignment operator 
  MyArray& operator= (  const MyArray& x )  {
    if ( this != &x)  {
      cout << __PRETTY_FUNCTION__ << endl;

      if ( m_arr!= nullptr ) delete [] m_arr;
      size_t n = m_size = x.m_size;
      m_arr = new int [n];
      copy (x.m_arr, x.m_arr+n, m_arr );
    }
    return *this;
  }

  // overload operator []
  inline T& operator[] (size_t i ) { return m_arr[i]; }
  inline const T& operator[] (size_t i ) const { return m_arr[i]; }

  // overload insertion operator <<
  friend ostream& operator<< (ostream& out, const MyArray& x ) {
    for (size_t i=0; i < x.m_size ; ++i ) { out << x[i] << " " ; } ;
    return out;
  }

};

void unitTest1()
{
  size_t n = 15;
  MyArray <int>* A = new MyArray<int> (n);
  MyArray <int>* B = new MyArray<int> (0);
  MyArray <int>* C = new MyArray<int> (*A);

  int arr[] = {7, 4,8,8,10,2,9,8,8,4,0,5,12,2,12} ;
  for (size_t i=0; i < n; ++i ) {
      // (*A)[i] = rand()%n;
      (*A)[i] =  arr[i] ;
  }

  *B = *A;
  *A = *A;
  cout << "A: " << *A << endl;
  cout << "B: " << *B << endl;
  cout << "C: " << *C << endl;
  delete A;
  delete B;
  delete C;
}

//int main(int argc, char* argv[]) { unitTest1(); }
