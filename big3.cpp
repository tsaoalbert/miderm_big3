#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <algorithm>
#include <iomanip>

using namespace std;

/*

  * Implement the Big-3 functions for deep copy needed for the following class that has dynamic allocated memory.
    
  * Look for the key words TODO to fill in the missing codes.
  
  * When done, turn on the unitTest to check your implementation.

  * The exptected output is shown below. 

A: 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12 
B: 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12 
C: 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 


*/

template<typename T>
class MyArray {
  friend ostream& operator<< (ostream& out, const MyArray & s )  ;
private:
  T* m_arr = nullptr ; 
  size_t m_size = 0 ;

public:
   // CSTR
   MyArray (  size_t n ) {
     // TODO: fill in the missing codes to allocate an array of size n
   };

   // Rules of Big 3: DSTR
   ~MyArray (  ) {
      // TODO: fill in the missing codes to delete allocated memory
   };

  // Rules of Big 3: Copy Cstr
  MyArray (  const MyArray& x )  {
      // TODO: fill in the missing codes to do deep copy
  }

  // Rules of Big 3: assignment operator 
  MyArray& operator= (  const MyArray& x )  {
      // TODO: fill in the missing codes to do deep copy
    return *this;
  }

  // overload operator []
  inline T& operator[] (size_t i ) { return m_arr[i]; }
  inline const T& operator[] (size_t i ) const { return m_arr[i]; }

  // overload insertion operator <<
  friend ostream& operator<< (ostream& out, const MyArray& x ) {
    for (size_t i=0; i < x.m_size ; ++i ) { out << x[i] << " " ; } ;
    return out;
  }

};


void unitTest () 
{
  size_t n = 15;
  MyArray <int>* A = new MyArray<int> (n);
  MyArray <int>* B = new MyArray<int> (0);
  MyArray <int>* C = new MyArray<int> (*A);
  int arr[] = {7, 4,8,8,10,2,9,8,8,4,0,5,12,2,12} ;
  for (size_t i=0; i < n; ++i ) { 
      // (*A)[i] = rand()%n; 
      (*A)[i] =  arr[i] ;
  }
  *B = *A;
  *A = *A;
  cout << "A: " << *A << endl;
  cout << "B: " << *B << endl;
  cout << "C: " << *C << endl;
  delete A;
  delete B;
  delete C;

}
int main(int argc, char* argv[]) { 
  // turn on unitTest once your implementation is ready;
  if (0) unitTest(); 
 }
